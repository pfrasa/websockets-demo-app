FROM openjdk:8u242-jdk-buster

WORKDIR /app
# First, download gradle and cache it
COPY gradle/ /app/gradle/
COPY gradlew /app/
## This command will implicity download gradle
RUN ./gradlew --version

# Copy build configuration and application code, build JAR
COPY settings.gradle.kts build.gradle.kts /app/
COPY src/ /app/src/
RUN ./gradlew bootJar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "build/libs/wsdemo-0.0.1-SNAPSHOT.jar"]
