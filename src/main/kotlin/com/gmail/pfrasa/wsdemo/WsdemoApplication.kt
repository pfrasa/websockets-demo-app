package com.gmail.pfrasa.wsdemo

import com.gmail.pfrasa.wsdemo.web.upload.UploadEndpoint
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry

@SpringBootApplication
@EnableWebSocket
class WsdemoApplication(private val uploadEndpoint: UploadEndpoint) {

    /* Configure websocket endpoints.
    *
    *  It's a bit unfortunate that, while WebSocket endpoints are configured explicitly in a central
    *  location, REST endpoints are only implicitly configured by adding new controllers.
    *  */
    @Configuration
    inner class WebSocketConfigurerImpl : WebSocketConfigurer {
        override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
            // For now, we'll allow all origins. If this endpoint should only be accessible to
            // trusted clients, this should be changed.
            registry.addHandler(uploadEndpoint, "/upload").setAllowedOrigins("*")
        }
    }
}

fun main(args: Array<String>) {
    runApplication<WsdemoApplication>(*args)
}
