package com.gmail.pfrasa.wsdemo.uploader

interface Uploader {
    /* Uploads an array of bytes to some location, using a provided filename. */
    fun upload(fileName: String, bytes: ByteArray): UploadStatus
}
