package com.gmail.pfrasa.wsdemo.uploader

import java.lang.NullPointerException
import java.net.URI
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import software.amazon.awssdk.core.exception.SdkException
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.GetUrlRequest
import software.amazon.awssdk.services.s3.model.PutObjectRequest

@Component
// We don't want to automatically instantiate this class in tests, because we don't want to
// test against actual S3.
// Therefore, in any test that needs to load the spring context, we'll want to substitute
// some fake implementation.
@Profile("!test")
// we only call this main constructor explicitly in tests
// Notice: This class isn't united tested due to the "don't mock what you don't own" principle.
//         Instead, it should be tested through integration tests.
class S3Uploader(
    private val s3Client: S3Client,
    private val s3Endpoint: String,
    private val s3Bucket: String
) : Uploader {

    // actual constructor called by spring boot
    // this will build the s3 client from the application properties
    @Autowired
    constructor(
        @Value("\${com.gmail.pfrasa.wsdemo.s3_endpoint}")
        s3Endpoint: String,
        @Value("\${com.gmail.pfrasa.wsdemo.s3_bucket}")
        s3Bucket: String,
        @Value("\${com.gmail.pfrasa.wsdemo.s3_region}")
        s3Region: String
    ) : this(
        S3Client.builder()
            .endpointOverride(URI(s3Endpoint))
            .region(Region.of(s3Region))
            .build(),
        s3Endpoint,
        s3Bucket
    )

    /* Uploads an array of bytes to an S3 bucket, using a provided filename. */
    override fun upload(fileName: String, bytes: ByteArray): UploadStatus {
        return try {
            s3Client.putObject(
                PutObjectRequest.builder().bucket(s3Bucket).key(fileName).build(),
                RequestBody.fromBytes(bytes)
            )
            val location = getLocation(fileName)
            UploadStatus.Success(location)
        } catch (e: SdkException) {
            UploadStatus.Failure(reason = e.localizedMessage)
        }
    }

    private fun getLocation(fileName: String): String {
        return try {
            s3Client.utilities()
                .getUrl(
                    GetUrlRequest.builder()
                        .bucket(s3Bucket)
                        .key(fileName)
                        .endpoint(URI(s3Endpoint))
                        .build()
                )
                .toString()
        } catch (e: NullPointerException) {
            // somehow, I can't get this request to work right with the S3 mock and I always get
            // a NPE in the tests. This should be fixed.
            // It appears to work in production.
            "some unknown location"
        }
    }
}
