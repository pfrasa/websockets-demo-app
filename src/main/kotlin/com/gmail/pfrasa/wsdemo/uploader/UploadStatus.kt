package com.gmail.pfrasa.wsdemo.uploader

sealed class UploadStatus {
    data class Success(val fileLocation: String) : UploadStatus()
    data class Failure(val reason: String) : UploadStatus()
}
