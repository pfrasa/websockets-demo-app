package com.gmail.pfrasa.wsdemo.web.uploadinfo

import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
class UploadInfoController {

    @GetMapping("/upload-info")
    fun getUploadInfo(): ResponseEntity<FileInfoDto> {
        val dummyFileInfo = FileInfoDto(fileName = "foo", fileSize = 300)
        return ResponseEntity.ok(dummyFileInfo)
    }
}
