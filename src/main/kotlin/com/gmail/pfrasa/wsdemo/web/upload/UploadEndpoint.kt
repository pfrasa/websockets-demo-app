package com.gmail.pfrasa.wsdemo.web.upload

import com.gmail.pfrasa.wsdemo.uploader.UploadStatus
import com.gmail.pfrasa.wsdemo.uploader.Uploader
import com.google.common.hash.Hashing
import org.springframework.stereotype.Component
import org.springframework.web.socket.BinaryMessage
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.BinaryWebSocketHandler

/* WebSocket endpoint to upload binary files. Only accepts binary messages */
@Component
class UploadEndpoint(private val uploader: Uploader) : BinaryWebSocketHandler() {

    override fun handleBinaryMessage(session: WebSocketSession, message: BinaryMessage) {
        session.sendMessage(TextMessage("You sent me a message of size ${message.payloadLength}!"))

        val bytes = message.payload.array()
        val fileName = Hashing.sha256().hashBytes(bytes).toString()

        val response = when (val result = uploader.upload(fileName, bytes)) {
            is UploadStatus.Success -> "Success! Uploaded message to ${result.fileLocation}"
            is UploadStatus.Failure -> "Failure! The following error occurred: ${result.reason}"
        }

        session.sendMessage(TextMessage(response))
    }
}
