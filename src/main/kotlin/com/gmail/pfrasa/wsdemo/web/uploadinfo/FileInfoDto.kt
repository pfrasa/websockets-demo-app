package com.gmail.pfrasa.wsdemo.web.uploadinfo

data class FileInfoDto(val fileName: String, val fileSize: Int)
