package com.gmail.pfrasa.wsdemo.acceptancetests

import com.adobe.testing.s3mock.junit5.S3MockExtension
import com.gmail.pfrasa.wsdemo.uploader.S3Uploader
import com.gmail.pfrasa.wsdemo.uploader.Uploader
import io.ktor.client.HttpClient
import io.ktor.client.features.websocket.WebSockets
import io.ktor.client.features.websocket.ws
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.http.cio.websocket.send
import io.ktor.util.KtorExperimentalAPI
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.fail
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.test.web.reactive.server.WebTestClient
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.CreateBucketRequest
import software.amazon.awssdk.services.s3.model.ListObjectsRequest

@KtorExperimentalAPI
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UploadTests {

    @LocalServerPort
    private var serverPort: Int = 0

    // clients (REST and WebSockets)
    @Autowired
    private lateinit var restClient: WebTestClient
    val websocketClient = HttpClient {
        install(WebSockets)
    }

    // S3 Mock config
    companion object {
        @RegisterExtension
        @JvmField
        val s3Mock: S3MockExtension =
            S3MockExtension.builder().silent().withSecureConnection(false).build()
    }

    private val s3Bucket = "testbucket"
    private val s3Client: S3Client = s3Mock.createS3ClientV2()

    @TestConfiguration
    class TestConfigurationImpl {
        @Primary
        @Bean
        fun s3MockUploader(): Uploader {
            return S3Uploader(
                s3Client = s3Mock.createS3ClientV2(),
                s3Endpoint = "http://mock_endpoint",
                s3Bucket = "testbucket"
            )
        }
    }

    @BeforeEach
    fun setUp() {
        s3Client.createBucket(CreateBucketRequest.builder().bucket(s3Bucket).build())
    }

    @Test
    fun uploadInfoEndpointReturnsValidJson() {
        restClient.get().uri("/upload-info").exchange()
            .expectStatus().is2xxSuccessful
            .expectBody()
            .jsonPath("$.fileName").isEqualTo("foo")
            .jsonPath("$.fileSize").isEqualTo(300)
    }

    @Test
    fun uploadEndpointDoesNotAcceptTextInputViaWebSocket() = runBlocking {
        websocketClient.ws(host = "127.0.0.1", port = serverPort, path = "/upload") {
            send("foobar")
            val closeReason = closeReason.await()
            assertThat(closeReason?.knownReason).isEqualTo(CloseReason.Codes.CANNOT_ACCEPT)
        }
    }

    @Test
    fun uploadEndpointUploadsBinaryInputToS3() = runBlocking {
        websocketClient.ws(host = "127.0.0.1", port = serverPort, path = "/upload") {
            val bytes = byteArrayOf(
                0xA1.toByte(), 0x2E.toByte(), 0x38.toByte(), 0xD4.toByte()
            )
            val data = Frame.Binary(fin = true, data = bytes)

            send(data)

            when (val firstResponse = incoming.receive()) {
                is Frame.Text -> assertThat(firstResponse.readText())
                    .isEqualTo("You sent me a message of size 4!")
                else -> fail("First response should have been a text message.")
            }

            when (val secondResponse = incoming.receive()) {
                is Frame.Text -> assertThat(secondResponse.readText())
                    .contains("Success")
                else -> fail("Second response should have been a text message.")
            }

            val objects = s3Client
                .listObjects(ListObjectsRequest.builder().bucket(s3Bucket).build())
                .contents()

            assertThat(objects.size).isEqualTo(1)
            assertThat(objects[0].size()).isEqualTo(4)
        }
    }
}
