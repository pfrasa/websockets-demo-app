package com.gmail.pfrasa.wsdemo

import com.gmail.pfrasa.wsdemo.uploader.UploadStatus
import com.gmail.pfrasa.wsdemo.uploader.Uploader
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary

@SpringBootTest
class WsdemoApplicationTests {

    @Test
    fun contextLoads() {
        // verify that the Spring Boot application loads correctly
        // Warning: We can't really test that the S3 uploader loads correctly because for that
        //          we'd need to make a request to S3. So, we override that bean configuration.
    }

    // specify a mock uploader that does nothing.
    @TestConfiguration
    class TestConfigurationImpl {
        @Primary
        @Bean
        fun mockUploader(): Uploader = object : Uploader {
            override fun upload(fileName: String, bytes: ByteArray): UploadStatus {
                return UploadStatus.Success("mock_location")
            }
        }
    }
}
