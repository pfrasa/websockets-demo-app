package com.gmail.pfrasa.wsdemo.web.upload

import com.gmail.pfrasa.wsdemo.uploader.UploadStatus
import com.gmail.pfrasa.wsdemo.uploader.Uploader
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentCaptor
import org.mockito.ArgumentMatchers.eq
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.Mockito.reset
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.springframework.web.socket.BinaryMessage
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession

class UploadEndpointTests {

    private val uploader: Uploader = mock(Uploader::class.java)
    private val session: WebSocketSession = mock(WebSocketSession::class.java)
    private val endpoint = UploadEndpoint(uploader = uploader)

    @BeforeEach
    @AfterEach
    fun resetMocks() {
        reset(uploader, session)
        `when`(uploader.upload(any(), any())).thenReturn(UploadStatus.Success("foobar"))
    }

    @Test
    fun endpointDoesntAcceptTextMessages() {
        val message = TextMessage("foo")
        endpoint.handleMessage(session, message)
        verify(session).close(any())
    }

    @Test
    fun endpointCommunicatesSizeOfReceivedMessageToSender() {
        val message1 = message(size = 3)
        endpoint.handleMessage(session, message1)
        verify(session).sendMessage(TextMessage("You sent me a message of size 3!"))

        val message2 = message(size = 10)
        endpoint.handleMessage(session, message2)
        verify(session).sendMessage(TextMessage("You sent me a message of size 10!"))
    }

    @Test
    fun endpointUploadsReceivedBytes() {
        val bytes = byteArrayOf(0xA1.toByte(), 0x2E.toByte(), 0x38.toByte(), 0xD4.toByte())
        val message = BinaryMessage(bytes)
        endpoint.handleMessage(session, message)
        verify(uploader).upload(any(), eq(bytes))
    }

    @Test
    fun endpointUploadsBytesWithTheSHAAsFilename() {
        val bytes = byteArrayOf(0xA1.toByte(), 0x2E.toByte(), 0x38.toByte(), 0xD4.toByte())
        val message = BinaryMessage(bytes)
        val expectedFileName = "f8bcb80da43c6fca951f119401f95a8a949a8253442d81cb37e712dc25962101"

        endpoint.handleMessage(session, message)

        verify(uploader).upload(eq(expectedFileName), any())
    }

    @Test
    fun ifUploadSucceedsThisIsCommunicatedToClient() {
        `when`(uploader.upload(any(), any())).thenReturn(UploadStatus.Success("http://fake.url"))

        val message = message(size = 9)
        endpoint.handleMessage(session, message)

        val arguments = ArgumentCaptor.forClass(TextMessage::class.java)
        verify(session, times(2)).sendMessage(arguments.capture())
        val messages = arguments.allValues.map { it.payload }
        assertThat(messages[1]).contains("Success!")
        assertThat(messages[1]).contains("http://fake.url")
    }

    @Test
    fun ifUploadFailsThisIsCommunicatedToClient() {
        `when`(uploader.upload(any(), any())).thenReturn(UploadStatus.Failure("fake reason"))

        val message = message(size = 12)
        endpoint.handleMessage(session, message)

        val arguments = ArgumentCaptor.forClass(TextMessage::class.java)
        verify(session, times(2)).sendMessage(arguments.capture())
        val messages = arguments.allValues.map { it.payload }
        assertThat(messages[1]).contains("Failure!")
        assertThat(messages[1]).contains("fake reason")
    }

    private fun message(size: Int): BinaryMessage {
        val bytes = ByteArray(size) { i -> i.toByte() }
        return BinaryMessage(bytes)
    }

    /**
     * Returns Mockito.any() as nullable type to avoid java.lang.IllegalStateException when
     * null is returned.
     *
     * See: https://stackoverflow.com/a/49152722
     */
    private fun <T> any(): T = Mockito.any<T>()

    /**
     * Returns Mockito.eq() as nullable type to avoid java.lang.IllegalStateException when
     * null is returned.
     *
     * See: https://stackoverflow.com/a/49152722
     */
    private fun <T> eq(value: T): T = Mockito.eq(value)
}
