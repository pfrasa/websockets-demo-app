const WebSocket = require("ws");
const fs = require("fs");
const socket = new WebSocket("ws://localhost:8080/upload");

const fileName = process.argv[2]
const data = fs.readFileSync(fileName)
let messagesReceived = 0

socket.on("message", (response) => {
    messagesReceived++;
    console.log(response);

    // the server is supposed to reply with two messages:
    // the first one acknowledges that it received the client's message
    // the second one should notify the client about the success of the upload
    if (messagesReceived >= 2) {
        socket.close()
        process.exit(0);
    }
})

socket.on("error", (error) => {
    console.log(`Error: ${error}`)
    socket.close()
    process.exit(0);
})

socket.on("close", (reason) => {
    console.log(`Socket closed: Code ${reason}`)
    console.log("See: https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent")
    process.exit(0);
})

socket.on("open", () => {
    socket.send(data);
})