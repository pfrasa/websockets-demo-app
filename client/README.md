# WebSocket Demo Dummy Client

Dummy client to test the WebSocket dummy application. It can upload files to S3 buckets.

## Install dependencies

With Node installed, run
```
npm install
```

## Run

Run the server (see the README of the parent directory) and then, from this directory, issue
```
node index.js path/to/some/file
```
