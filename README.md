# WebSockets Demo Server

This is a demo server written with Spring Boot and Kotlin that showcases the use of Websockets to upload data to S3 buckets.

## Running the server

First, set up S3. The easiest way to do so is to use [S3Mock](https://github.com/adobe/S3Mock). You can simply run the following command to spin up the mock:
```
docker run -e initialBuckets=testbucket -p 9090:9090 -p 9191:9191 -t adobe/s3mock:latest
```

If you want to use an actual S3 bucket, you'll need to change or override the properties in `src/main/resources/application.yml`.

Then, run the server with
```
./gradlew bootRun
```

In order to test the server, you can use the mock client in `client/`.

## Development

This app uses [gradle](https://gradle.org/) to manage dependencies and tasks. To run the application, enter:
```
./gradlew bootRun
```
To run tests:
```
./gradlew check
```
To create a bootable JAR:
```
./gradlew bootJar
```

**Important**: This project uses [dependency version locking](https://docs.gradle.org/current/userguide/dependency_locking.html), so whenever you update dependencies, you'll need to use the `--write-locks` flag with your `gradle` commands.

## CI
This project is hosted on [Gitlab](https://gitlab.com/pfrasa/websockets-demo-app) and uses its CI tooling. The pipelines are configured in the `.gitlab-ci.yml` file.

Deployment is not implemented yet for this project, but gitlab will build a docker image and host it in its registry; this docker image could then be deployed anywhere.

To build the docker image manually, run
``` 
docker build -t ws-demo .
```
and then run it with
```
docker run -p 8080:8080 ws-demo
```

However, that docker container will not be able to communicate to a container running S3Mock.

## Missing parts
The following features haven't yet been implemented:

* A HTTP endpoint that returns the status of the last uploaded file. Currently, there's only a dummy endpoint that returns fake data.
* Similarly, the client doesn't yet check that endpoint.
* Possibly a way for the docker image to communicate to S3Mock.
* Testing the whole thing with an actual S3 bucket.
* Deployment via Kubernetes.
* Additionally, there's a bug in the S3 uploader within integration tests; somehow it's impossible to retrieve the location of the uploaded file.
